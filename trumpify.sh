#!/bin/bash

OPTIND=1 # Reset getopts

wordwrap() { cat; }
colorize() { cat; }
indent() { sed 's/^/\ \ \ \ /'; }

usage() { echo "Usage:
    -h This help message
    -l Line length for word wrap (default: 70)
    -b Box type                  (default: nuke)
         *requires 'boxes'
         Use 'none' for no box.
         Use 'random' for a random box.
         Run 'boxes -l' to see all box types.
    -c Color to use [0-63]       (default: 1)
         *requires 'lolcat'
         Use 'none' for no color.
    -s Rainbow color spread      (default: 20)
         *requires 'lolcat'
    -u Update database
         *requires 'curl'
    -i Print install command for helper packages"
}

parseopts() {
  while getopts "h?l:b:c:s:ui" OPT; do
    case "${OPT}" in
      h|\?)
        usage
        exit 0
        ;;
      l)
        LINELENGTH=${OPTARG}
        ;;
      b)
        BOXTYPE=${OPTARG}
        ;;
      c)
        COLOR=${OPTARG}
        ;;
      s)
        SPREAD=${OPTARG}
        ;;
      u)
        echo "Updating database..."
        updatedb
        exit 0
        ;;
      i)
        if [ "$(which apt)" ]; then echo -n "apt install "
        elif [ "$(which yum)" ]; then echo -n "yum install "
        elif [ "$(which yast)" ]; then echo -n "yast -i "
        else echo "Error: Could not guess package manager"; fi
        echo "curl boxes lolcat"
        exit 0
        ;;
      *)
        echo "Error parsing arguments..."
        usage
        exit 1
    esac
  done
}

updatedb() {
  local DATABASE="$1"
  if [ ! "$(which curl)" ]; then
    echo "Error: Cannot download database, please install 'curl'"
    exit 1
  fi
  curl https://www.shortlist.com/news/most-ridiculous-trump-quotes-ever -H 'User-Agent: Mozilla/5.0 ...' > /tmp/trumpify.dnld
  cat /tmp/trumpify.dnld | tr -d "\n\r" | sed 's/<h3>/\n<h3>/g' | sed 's/<h3>On \(.*\)<\/h3><p>\([^<]*\)<\/p>/\1\n\2\n/g' | grep -v "</" > ${DATABASE}
  rm -f /tmp/trumpify.dnld
}

loaddb() {
  local DATABASE="$1"
  mapfile -t COMMENTS < <(cat ${DATABASE})
}

createmsg() {
  #while [ $(expr length "${TOPIC}") -ge $(expr length "${QUOTE}") ]; do
    ID=$((RANDOM % $[${#COMMENTS[@]}/2]))
    TOPIC="${COMMENTS[$[${ID}*2]]}"
    QUOTE="$(echo ${COMMENTS[$[${ID}*2+1]]} | wordwrap)"
  #done

  #MESSAGE="Trump on ${TOPIC}:\n$(echo ${QUOTE} | indent)"

  AUTHOR=$(printf '%*s' 70 "-Donald J. Trump")
  CONTEXT=$(printf '%*s' 70 "(on ${TOPIC})")
  MESSAGE="${QUOTE}\n"
  if [ $(expr length "$(echo -e "${QUOTE}" | tail -n1)") -ge $[${LINELENGTH}-20] ]; then
    MESSAGE="${MESSAGE}\n"
  fi
  MESSAGE="${MESSAGE}${AUTHOR}\n"
  MESSAGE="${MESSAGE}${CONTEXT}"
}

printmsg() {
  echo
  local MESSAGE="$1"
  if [ "$(which boxes)" ] && [ "${BOXTYPE}" != "none" ]; then
    if [ "${BOXTYPE}" == "random" ]; then
      BOXTYPE=$(boxes -l | grep -B 1 "coded by" |  sed 's/--//g' | sed -n -E '/^\s*\S+\s*$/p' | shuf -n1)
    fi
    boxes -d ${BOXTYPE} <<< "$(echo -e "${MESSAGE}")" | colorize
  else
    echo -e "${MESSAGE}" | indent | colorize
  fi
  echo
}

#################################### MAIN ######################################

# default settings
DATABASE=~/.trumpify.db
LINELENGTH=70
#BOXTYPE=nuke
BOXTYPE=random
COLOR=1
SPREAD=20

parseopts "$@"

if [ "$(which fold)" ]; then
  wordwrap() { fold -s -w ${LINELENGTH}; }
fi

if [ "$(which lolcat)" ] && [ "${COLOR}" != "none" ]; then
  colorize() { lolcat -S ${COLOR} -p ${SPREAD}; }
fi

if [ ! -f ${DATABASE} ]; then
  updatedb "${DATABASE}"
fi

loaddb "${DATABASE}"
createmsg
printmsg "${MESSAGE}"

