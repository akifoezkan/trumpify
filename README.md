# Trumpify Your Shell Greeter

![How it looks](./demo.png)

## Install Helper Packages
`sudo $(./trumpify.sh -i)`

## Install Trumpify to Bash
`./install_to_bash.sh`

## Update Database
`./trumpify.sh -u`

